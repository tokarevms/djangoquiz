from django.contrib import messages
from django.shortcuts import redirect


def moderator_required(view_func):
    def decorator(request, *args, **kwargs):
        u = request.user
        if u.is_superuser or u.groups.filter(name__contains='Moderator').exists():
            return view_func(request, *args, **kwargs)
        messages.warning(request, 'Требуются права модератора!')
        return redirect('users:login')
    return decorator
