from django.contrib.auth.views import LogoutView
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.shortcuts import redirect, get_object_or_404
from django.db.models import Count
from .decorators import moderator_required
from quiz.models import TakenQuiz


class CustomLogoutView(LogoutView):
    def dispatch(self, request, *args, **kwargs):
        unfinished_quizzes = TakenQuiz.objects.filter(creator=request.user, score=None)
        if unfinished_quizzes:
            unfinished_quizzes.update(score=0)
        return super().dispatch(request, *args, **kwargs)


class CustomSignupView(generic.CreateView):
    model = User
    form_class = UserCreationForm
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('quiz:home')


@method_decorator(moderator_required, name='dispatch')
class UsersWithReportsView(generic.ListView):
    template_name = 'users/users_with_reports.html'
    context_object_name = 'users_with_reports'

    def get_queryset(self):
        return User.objects.exclude(taken_quizzes=None)\
            .annotate(taken_quizzes_count=Count('taken_quizzes'))


@method_decorator(moderator_required, name='dispatch')
class UserResultsView(generic.ListView):
    template_name = 'users/user_results.html'
    context_object_name = 'user_taken_quizzes'

    def get_queryset(self):
        return TakenQuiz.objects.filter(creator__id=self.kwargs['pk'])\
            .select_related('quiz')\
            .order_by('created_on')

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)
        context_data['user'] = get_object_or_404(User, pk=self.kwargs['pk'])
        return context_data
