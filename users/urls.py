from django.urls import path, include

from . import views

app_name = 'users'
urlpatterns = [
    path('', views.UsersWithReportsView.as_view(), name='users_with_reports'),
    path('<int:pk>/results/', views.UserResultsView.as_view(), name='user_results'),
    path('signup/', views.CustomSignupView.as_view(), name='signup'),
    path('logout/', views.CustomLogoutView.as_view(), name='logout'),
    path('', include('django.contrib.auth.urls'))
]
