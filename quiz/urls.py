from django.urls import path

from . import views

app_name = 'quiz'
urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('category/<int:pk>/', views.CategoryView.as_view(), name='category_quizzes'),
    path('quiz/<int:pk>/', views.QuizView.as_view(), name='quiz_details'),
    path('quiz/<int:pk>/take/', views.TakeQuizView.as_view(), name='take_quiz'),
    path('result/<int:pk>/', views.ResultDetailsView.as_view(), name='result_details'),
    path('results/', views.UserResultsView.as_view(), name='results'),

    path('quizzes/change/', views.QuizListView.as_view(), name='quiz_change_list'),
    path('quiz/add/', views.QuizCreateView.as_view(), name='quiz_add'),
    path('quiz/<int:pk>/change/', views.QuizUpdateView.as_view(), name='quiz_change'),
    path('quiz/<int:pk>/delete/', views.QuizDeleteView.as_view(), name='quiz_delete'),

    path('categories/change/', views.CategoryListView.as_view(), name='category_change_list'),
    path('category/add/', views.CategoryCreateView.as_view(), name='category_add'),
    path('category/<int:pk>/change/', views.CategoryUpdateView.as_view(), name='category_change'),
    path('category/<int:pk>/delete/', views.CategoryDeleteView.as_view(), name='category_delete'),

    path('quiz/<int:pk>/question/add/', views.QuestionAddView.as_view(), name='question_add'),
    path('quiz/<int:quiz_pk>/question/<int:question_pk>/', views.question_change, name='question_change'),
    path('quiz/<int:quiz_pk>/question/<int:question_pk>/delete/', views.QuestionDeleteView.as_view(), name='question_delete')
]
