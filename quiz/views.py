from django.views import generic
from .models import Category, Quiz, Question, TakenQuiz, Choice
from django.shortcuts import get_object_or_404, render, redirect, reverse
from django.db.models import QuerySet, Count
from . import forms
from json import dumps, loads
from django.contrib import messages
from django.urls import reverse_lazy
from django.forms import inlineformset_factory
from users.decorators import moderator_required
from django.utils.decorators import method_decorator
from django.utils import timezone


class HomeView(generic.ListView):
    template_name = 'quiz/home.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.objects.annotate(quizzes_count=Count('quizzes'))


class CategoryView(generic.ListView):
    template_name = 'quiz/category.html'
    context_object_name = 'quizzes'

    def get_category(self):
        return get_object_or_404(Category, pk=self.kwargs['pk'])

    def get_queryset(self):
        return self.get_category().quizzes.annotate(questions_count=Count('questions'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.get_category()
        return context


class QuizView(generic.DetailView):
    template_name = 'quiz/quiz.html'
    context_object_name = 'quiz'

    def get_queryset(self):
        return Quiz.objects.select_related('creator', 'category')\
            .filter(pk=self.kwargs['pk'])\
            .annotate(questions_count=Count('questions'))


class TakeQuizView(generic.FormView):
    template_name = 'quiz/take_quiz.html'
    http_method_names = ['post']

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        form = context_data['form']
        context_data.update({
            'question_num': self.request.session['quiz_data']['current_pos'] + 1,
            'questions_count': self.request.session['quiz_data']['total_questions'],
            'question': form.question,
            'quiz': form.question.quiz,
            'category': form.question.quiz.category,
        })
        return context_data

    def post(self, request, *args, **kwargs):
        quiz_data = request.session.get('quiz_data')
        if quiz_data:
            if quiz_data['quiz'] != kwargs['pk']:
                messages.error(request, 'Вы еще не завершили этот опрос.')
                return redirect('quiz:quiz_details', quiz_data['quiz'])
            form = self.get_form(data=request.POST)
            if form.is_valid():
                return self.form_valid(form)
            return self.form_invalid(form)
        return self.first_form(request, kwargs)

    def first_form(self, request, kwargs):
        quiz = get_object_or_404(Quiz, pk=kwargs['pk'])
        questions = list(quiz.questions.values_list('id', flat=True))
        taken_quiz = TakenQuiz.objects.create(
            creator=request.user,
            quiz=quiz
        )
        request.session['quiz_data'] = {
            'quiz': kwargs['pk'],
            'questions': questions,
            'current_pos': 0,
            'answers': {},
            'taken_quiz': taken_quiz.id,
            'total_questions': len(questions)
        }
        return self.render_to_response(self.get_context_data())

    def get_form(self, **kwargs):
        quiz_data = self.request.session['quiz_data']
        question = Question.objects \
            .select_related('quiz', 'quiz__category')\
            .get(pk=quiz_data['questions'][quiz_data['current_pos']])
        return {
            'R': forms.TakeQuizRadioForm,
            'C': forms.TakeQuizMultiForm,
            'T': forms.TakeQuizTextForm
        }[question.question_type](question, **kwargs)

    def form_valid(self, form):
        quiz_data = self.request.session['quiz_data']
        questions = quiz_data['questions']
        current_pos = quiz_data['current_pos']
        answer = form.cleaned_data['answer']
        if isinstance(answer, Choice):
            quiz_data['answers'][questions[current_pos]] = {'pk': form.cleaned_data['answer'].id}
        elif isinstance(answer, QuerySet):
            quiz_data['answers'][questions[current_pos]] = {'pks': list(form.cleaned_data['answer'].values_list('id', flat=True))}
        else:
            quiz_data['answers'][questions[current_pos]] = {'txt': form.cleaned_data['answer']}
        self.request.session['quiz_data']['answers'] = quiz_data['answers']
        if current_pos >= len(questions) - 1:
            score = self.calculate_results(quiz_data['answers'])
            taken_quiz = TakenQuiz.objects.select_related('quiz', 'quiz__category')\
                .get(id=quiz_data['taken_quiz'])
            taken_quiz.answers = dumps(quiz_data['answers'])
            taken_quiz.time = timezone.now() - taken_quiz.created_on
            taken_quiz.score = int(score)
            taken_quiz.save()
            del self.request.session['quiz_data']
            return render(self.request, 'quiz/finished_quiz.html', {
                'taken_quiz': taken_quiz,
                'category': taken_quiz.quiz.category,
                'quiz': taken_quiz.quiz
            })
        else:
            current_pos += 1
            quiz_data['current_pos'] = current_pos
            self.request.session['quiz_data'] = quiz_data
            return self.render_to_response(self.get_context_data())

    @staticmethod
    def calculate_results(answers):
        for question_pk, question_answers in answers.items():
            if 'pk' in question_answers:
                question_answers['correct'] = True if Choice.objects.get(pk=question_answers['pk']).correct is True else False
            elif 'pks' in question_answers:
                correct_answers = set(Question.objects.get(pk=question_pk).choices.filter(correct=True).values_list('id', flat=True))
                given_answers = set(question_answers['pks'])
                question_answers['correct'] = True if correct_answers == given_answers else False
            elif 'txt' in question_answers:
                correct_answers = Question.objects.get(pk=question_pk).choices.filter(correct=True).first().body
                question_answers['correct'] = True if correct_answers == question_answers['txt'] else False
        return round(100 * sum(1 for elem in answers.values() if elem['correct']) / len(answers))


class ResultDetailsView(generic.DetailView):
    template_name = 'quiz/result_details.html'
    context_object_name = 'taken_quiz'
    model = TakenQuiz

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        answers_data = self.object.answers
        if answers_data:
            answers = loads(answers_data)
            questions = Question.objects.filter(pk__in=answers.keys())
            context['questions'] = []
            for question in questions:
                q_answers = answers[str(question.id)]
                if 'pk' in q_answers:
                    prepared_answers = [Choice.objects.get(pk=q_answers['pk'])]
                elif 'pks' in q_answers:
                    prepared_answers = list(Choice.objects.filter(pk__in=q_answers['pks']))
                elif 'txt' in q_answers:
                    prepared_answers = [{'body': q_answers['txt']}]
                context['questions'].append({
                    'object': question,
                    'answers': prepared_answers,
                    'correct': q_answers['correct']
                })
        else:
            context['questions'] = None
        return context


class UserResultsView(generic.ListView):
    template_name = 'quiz/taken_quizzes.html'
    context_object_name = 'taken_quizzes'

    def get_queryset(self):
        return self.request.user.taken_quizzes.exclude(score=None)


@method_decorator(moderator_required, name='dispatch')
class CategoryListView(generic.ListView):
    model = Category
    ordering = ('name', 'created_on')
    context_object_name = 'categories'
    template_name = 'quiz/category/category_change_list.html'

    def get_queryset(self):
        return self.request.user.created_categories\
            .annotate(quizzes_count=Count('quizzes', distinct=True))


@method_decorator(moderator_required, name='dispatch')
class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'quiz/category/category_add.html'
    fields = ('name', 'description')

    def form_valid(self, form):
        category = form.save(commit=False)
        category.creator = self.request.user
        category.save()
        messages.success(self.request, 'Категория успешно добавлена!')
        return redirect('quiz:category_change', category.pk)


@method_decorator(moderator_required, name='dispatch')
class CategoryUpdateView(generic.UpdateView):
    model = Category
    context_object_name = 'category'
    template_name = 'quiz/category/category_change.html'
    fields = ('name', 'description')

    def get_queryset(self):
        return self.request.user.created_categories.all()

    def get_success_url(self):
        return reverse('quiz:quiz_change', kwargs={'pk': self.object.pk})


@method_decorator(moderator_required, name='dispatch')
class CategoryDeleteView(generic.DeleteView):
    model = Category
    context_object_name = 'category'
    template_name = 'quiz/category/category_delete_confirmation.html'
    success_url = reverse_lazy('quiz:quiz_change_list')

    def delete(self, request, *args, **kwargs):
        category = self.get_object()
        if category.quizzes.exists():
            messages.error(request, 'Вы не можете удалить непустую категорию %s!' % category.name)
            return redirect('quiz:quiz_change_list')
        messages.success(request, 'Категория %s успешно удалена!' % category.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.created_categories.all()


@method_decorator(moderator_required, name='dispatch')
class QuizListView(generic.ListView):
    model = Quiz
    ordering = ('created_on', 'subject')
    context_object_name = 'quizzes'
    template_name = 'quiz/quiz/quiz_change_list.html'

    def get_queryset(self):
        return self.request.user.created_quizzes\
            .annotate(questions_count=Count('questions', distinct=True))\
            .annotate(taken_count=Count('taken_quizzes', distinct=True))


@method_decorator(moderator_required, name='dispatch')
class QuizCreateView(generic.CreateView):
    model = Quiz
    template_name = 'quiz/quiz/quiz_add.html'
    fields = ('subject', 'description', 'category')

    def form_valid(self, form):
        quiz = form.save(commit=False)
        quiz.creator = self.request.user
        quiz.save()
        messages.success(self.request, 'Опрос успешно добавлен!')
        return redirect('quiz:quiz_change', quiz.pk)


@method_decorator(moderator_required, name='dispatch')
class QuizUpdateView(generic.UpdateView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'quiz/quiz/quiz_change.html'
    fields = ('subject', 'description', 'category')

    def get_context_data(self, **kwargs):
        kwargs['questions'] = self.get_object().questions.annotate(answers_count=Count('choices'))
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return self.request.user.created_quizzes.all()

    def get_success_url(self):
        return reverse('quiz:quiz_change', kwargs={'pk': self.object.pk})


@method_decorator(moderator_required, name='dispatch')
class QuizDeleteView(generic.DeleteView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'quiz/quiz/quiz_delete_confirmation.html'
    success_url = reverse_lazy('quiz:quiz_change_list')

    def delete(self, request, *args, **kwargs):
        quiz = self.get_object()
        messages.success(request, 'Опрос %s успешно удален!' % quiz.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.created_quizzes.all()


@method_decorator(moderator_required, name='dispatch')
class QuestionAddView(generic.CreateView):
    form_class = forms.QuestionForm
    model = Question
    template_name = 'quiz/quiz/question_add.html'

    def get_context_data(self, **kwargs):
        kwargs['quiz'] = get_object_or_404(Quiz, pk=self.kwargs['pk'])
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        question = form.save(commit=False)
        quiz = get_object_or_404(Quiz, pk=self.kwargs['pk'])
        question.quiz = quiz
        question.save()
        messages.success(self.request, 'Добавьте варианты ответов к вопросу.')
        return redirect('quiz:question_change', quiz.pk, question.pk)


@moderator_required
def question_change(request, quiz_pk, question_pk):
    quiz = get_object_or_404(Quiz, pk=quiz_pk, creator=request.user)
    question = get_object_or_404(Question, pk=question_pk, quiz=quiz)
    answer_formset = inlineformset_factory(
        Question,
        Choice,
        formset=forms.BaseChoiceInlineFormSet,
        fields=('body', 'correct'),
        min_num=1,
        extra=2,
        validate_min=True,
        max_num=10,
        validate_max=True
    )
    if request.method == 'POST':
        form = forms.QuestionForm(request.POST, instance=question)
        formset = answer_formset(request.POST, instance=question)
        if form.is_valid() and formset.is_valid():
            form.save()
            formset.save()
            messages.success(request, 'Вопрос и ответы успешно сохранены !')
            return redirect('quiz:quiz_change', quiz.pk)
    else:
        form = forms.QuestionForm(instance=question)
        formset = answer_formset(instance=question)
    return render(request, 'quiz/quiz/question_change.html', {
        'quiz': quiz,
        'question': question,
        'form': form,
        'formset': formset
    })


@method_decorator(moderator_required, name='dispatch')
class QuestionDeleteView(generic.DeleteView):
    model = Question
    context_object_name = 'question'
    template_name = 'quiz/quiz/question_delete_confirmation.html'
    pk_url_kwarg = 'question_pk'

    def get_context_data(self, **kwargs):
        question = self.get_object()
        kwargs['quiz'] = question.quiz
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        question = self.get_object()
        messages.success(request, 'Вопрос %s был успешно удален!' % question.subject)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return Question.objects.filter(quiz__creator=self.request.user)

    def get_success_url(self):
        question = self.get_object()
        return reverse('quiz:quiz_change', kwargs={'pk': question.quiz_id})
