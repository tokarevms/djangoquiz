from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(
        max_length=100,
        unique=True
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='created_categories'
    )
    description = models.TextField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('quiz:category_quizzes', args=[str(self.id)])


class Quiz(models.Model):
    creator = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        related_name='created_quizzes'
    )
    created_on = models.DateTimeField(auto_now_add=True)
    subject = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    category = models.ForeignKey(
        Category,
        on_delete=models.PROTECT,
        related_name='quizzes'
    )

    def __str__(self):
        return self.subject

    def get_absolute_url(self):
        return reverse('quiz:quiz_details', args=[str(self.id)])


class Question(models.Model):
    subject = models.CharField(max_length=100)
    body = models.TextField(max_length=1000)
    explanation = models.TextField(
        max_length=500,
        blank=True
    )
    question_type = models.CharField(
        max_length=1,
        choices=(
            ('R', 'Radio Button'),
            ('C', 'Checkbox'),
            ('T', 'Text field')
        ),
        default='R'
    )
    created_on = models.DateTimeField(auto_now_add=True)
    quiz = models.ForeignKey(
        Quiz,
        on_delete=models.CASCADE,
        related_name='questions'
    )

    def __str__(self):
        return self.subject


class Choice(models.Model):
    body = models.TextField(max_length=500)
    created_on = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='choices',
        null=True
    )
    correct = models.NullBooleanField()

    def __str__(self):
        return self.body


class TakenQuiz(models.Model):
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='taken_quizzes'
    )
    created_on = models.DateTimeField(auto_now_add=True)
    time = models.DurationField(null=True)
    quiz = models.ForeignKey(
        Quiz,
        on_delete=models.CASCADE,
        related_name='taken_quizzes'
    )
    score = models.FloatField(null=True)
    answers = models.TextField(blank=True)

    def __str__(self):
        return '{} taken by {} at {}, score: {}'.format(self.quiz, self.creator, self.created_on, self.score)

    def get_absolute_url(self):
        return reverse('quiz:result_details', args=[str(self.id)])
