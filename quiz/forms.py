from django import forms
from . import models
from django.core.exceptions import ValidationError


class BaseTakeQuizForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.question = question
        self.fields['answer'].queryset = question.choices.order_by('body')


class TakeQuizRadioForm(BaseTakeQuizForm):
    answer = forms.ModelChoiceField(
        queryset=models.Choice.objects.none(),
        widget=forms.RadioSelect(),
        required=True,
        empty_label=None,
        label='Варианты ответов'
    )


class TakeQuizMultiForm(BaseTakeQuizForm):
    answer = forms.ModelMultipleChoiceField(
        queryset=models.Choice.objects.none(),
        widget=forms.CheckboxSelectMultiple(),
        required=True,
        label='Варианты ответов'
    )


class TakeQuizTextForm(BaseTakeQuizForm):
    answer = forms.CharField(label='Введите ответ', widget=forms.Textarea(attrs={'cols': 20, 'rows': 2}))


class QuestionForm(forms.ModelForm):
    class Meta:
        model = models.Question
        fields = ('subject', 'body', 'explanation', 'question_type')


class BaseChoiceInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()
        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Отметьте хотя бы один верный ответ.', code='no_correct_answer')
