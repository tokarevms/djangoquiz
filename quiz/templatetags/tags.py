from django import template
from datetime import timedelta

register = template.Library()


@register.filter(name='is_moderator')
def has_group(user):
    return user.is_superuser or user.groups.filter(name__contains='Moderator').exists()


@register.filter(name='to_mins')
def delta(delta):
    if isinstance(delta, timedelta):
        return delta.seconds//60
    return ''
