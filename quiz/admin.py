from django.contrib import admin
from nested_admin.nested import NestedModelAdmin, NestedStackedInline, NestedTabularInline


from . import models


class ChoicesInline(NestedStackedInline):
    model = models.Choice
    extra = 0


class QuestionInline(NestedTabularInline):
    model = models.Question
    extra = 0
    inlines = (ChoicesInline,)


@admin.register(models.Category)
class CategoryAdmin(NestedModelAdmin):
    list_display = ('name', 'creator', 'created_on')


@admin.register(models.Quiz)
class QuizAdmin(NestedModelAdmin):
    list_display = ('subject', 'creator', 'created_on', 'category')
    list_filter = ('created_on', 'category')
    inlines = (QuestionInline, )


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('subject', 'created_on', 'question_type')
    list_filter = ('question_type',)


@admin.register(models.TakenQuiz)
class TakenQuizAdmin(admin.ModelAdmin):
    list_display = ('quiz', 'creator', 'created_on', 'score', 'time')
    list_filter = ('creator', 'quiz', 'score')
